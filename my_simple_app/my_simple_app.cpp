/****************************************************************************
 *
 *   Copyright (c) 2012-2019 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file my_simple_app.c
 * A simple app for extern spi bus ljj_spi
 *
 * @author 健宝 	<JohnLiuljj@163.com>
 */

#include <px4_platform_common/px4_config.h>
#include <px4_platform_common/tasks.h>
#include <px4_platform_common/posix.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>

#include <uORB/uORB.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>

#include <uORB/Publication.hpp>
#include <uORB/Subscription.hpp>
#include <uORB/SubscriptionCallback.hpp>
#include <uORB/topics/muc_recive.h>
#include <uORB/topics/muc_send.h>
#include <uORB/topics/muc_send.h>


extern "C" __EXPORT int my_simple_app_main(int argc, char *argv[]);




int my_simple_app_main(int argc, char *argv[])
{
	PX4_INFO("Good morning, fucking nice day !");
	uORB::Publication<muc_send_s>	_muc_send_pub{ORB_ID(muc_send)};
	uORB::Subscription _spi_recive_sub{ORB_ID(muc_recive)};

	struct muc_recive_s	spi_recive{};
	struct muc_send_s	spi_send{};
	static uint16_t num = 0;

	for (int i = 0; i < 12; i++) {

		PX4_INFO("This is the %dth num of 0-12 cycle ", i);
		uint64_t timestamp_us = hrt_absolute_time();

		spi_send.timestamp = timestamp_us;
		spi_send.reg = 0x99;
		spi_send.value = i*300;
		_muc_send_pub.publish(spi_send);

		if (_spi_recive_sub.updated()) {
			if (_spi_recive_sub.copy(&spi_recive)) {
				num = spi_recive.value;
			}
		}
		PX4_INFO("send %d , recive %d", spi_send.value, num);
		px4_sleep(2);
	}
	PX4_INFO("exiting");

	return 0;
}
